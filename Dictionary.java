package com.jaisnain.wordDictionary;

import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Dictionary {

	public static void main(String[] args) {
		CopyOnWriteArrayList<String> dictionary = new CopyOnWriteArrayList<String>();

		try{
			BufferedReader fileReader = new BufferedReader(new FileReader("inputFile.txt"));

			//taking input from user
			BufferedReader userInputReader = new BufferedReader(new InputStreamReader(System.in));
			int i = 0;
			dictionary = initializeDictionary(fileReader,dictionary);
			do{
				System.out.println("0 - Exit");
				System.out.println("1 - Search");
				System.out.println("2 - Insert");
				System.out.println("3 - Display all elements in the dictionary");
				System.out.print("Enter your choice: ");
				try{
					i = Integer.parseInt(userInputReader.readLine());
					if(i == 0){
						System.exit(0);
					}else if(i == 1){
						System.out.print("Word to be searched: ");
						String inputFromUser = userInputReader.readLine().trim().toLowerCase();
						for(String eachWord : inputFromUser.split("\\s*[^a-zA-Z]+\\s*")){
							search(eachWord,dictionary);
						}
					}else if(i == 2){
						System.out.print("Enter word to be inserted in dictionary : ");
						insertApproriateWordsInDictionary(dictionary,userInputReader.readLine().trim().toLowerCase());
					}else if(i == 3){
						System.out.print("\nDictionary size is " + dictionary.size() + "\nEnter the range of words you want to see between 0-"+dictionary.size()+": \nfrom : ");
						String fromRange = userInputReader.readLine().trim();
						if(fromRange.matches("\\d+")){
							int from = Integer.parseInt(fromRange);
							System.out.print("to : ");
							String toRange = userInputReader.readLine().trim();
							if(toRange.matches("\\d+")){
								int to = Integer.parseInt(toRange);
								if(from >= 0 && to <= dictionary.size() && from <= to){
									System.out.println(printWordsInDictionary(from,to, dictionary));
								}else{
									System.out.println("Range is out of bound");
								}
							}else{
								System.out.println("Invalid input!");
							}
						}else{
							System.out.println("Invalid input!");
						}
					}else{
						System.out.println("Invalid input!");
					}
				}catch(IOException e){
					System.out.println("Invalid input!");
				}
			}while(i != 0);


		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private static List<String> printWordsInDictionary(int from, int to,CopyOnWriteArrayList<String> dictionary) {
		return (dictionary.subList(from,to));
	}

	private static void search(String word, CopyOnWriteArrayList<String> dictionary) {
		//		System.out.println("Searching...");
		word = cleanWord(word);
		if(dictionary.contains(word)){
			System.out.println("'"+word + "' is found in dictionary!");
		}else{
			System.out.println("'"+word + "' not found in dictionary! Checking for any similar word...");
			Iterator<String> itr = dictionary.iterator();
			boolean similarFound = false;

			while(itr.hasNext()){
				String wordInDictionary = itr.next();
				if(wordInDictionary.matches(".*"+word+".*")){
					similarFound = true;
					System.out.println(wordInDictionary);
				}
			}
			if(!similarFound){
				System.out.println("Sorry no simiar words found!");
			}
		}
	}

	private static String cleanWord(String word) {
		//		System.out.println("Cleaning data...");
		// Cleaning data
		word = word.replace(".", "");
		return word;
	}

	private static CopyOnWriteArrayList<String> initializeDictionary(BufferedReader fileReader,
			CopyOnWriteArrayList<String> dictionary) {
		String eachLine = null;
		System.out.println("Initializing dictionary...");
		try {

			while((eachLine = fileReader.readLine()) != null){
				insertApproriateWordsInDictionary(dictionary,eachLine);
			}
//			System.out.println(dictionary);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dictionary;
	}

	private static void insertApproriateWordsInDictionary(CopyOnWriteArrayList<String> dictionary, String eachLine) {
		String[] wordsInLine = eachLine.split("\\s*[^a-zA-Z]+\\s*");

		for(String eachWord : wordsInLine){
			eachWord = cleanWord(eachWord).toLowerCase();
			if(!dictionary.contains(eachWord)){
				dictionary.addIfAbsent(eachWord);
			}
		}	
	}
}